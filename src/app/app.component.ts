import { Component, OnInit, Input } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { TodoItem, FETCHTODO, ADDTODO, UPDATETODO, REMOVETODO } from './todos.state';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  closeResult: string;
 	@Input() indexInDataSet; 
 	@Input() valueInDataSet;
	
  @Select('todos.dataset') todos: Observable<TodoItem[]>;

  constructor(private store: Store, private modalService: NgbModal,private fb:FormBuilder) {

    this.store.dispatch(new FETCHTODO()).subscribe(state => {
    });
  }

  addTodo(input) {
  	if (!input.value)
  	{
  		return;
  	}
    this.store.dispatch(new ADDTODO(input.value)).subscribe(state => {
      input.value = '';
    });
  }

  openUpdateTodoModal(modalForUpdateTodoItem, index, value) {
		this.modalService.open(modalForUpdateTodoItem);
    this.indexInDataSet = index;
    this.valueInDataSet = value;
  }

  updateTodo(input) {
		this.store.dispatch(new UPDATETODO(input.value, input.getAttribute('data-index'))).subscribe(state => {
		});	
  }

  removeTodo(indexOfItem) {
    this.store.dispatch(new REMOVETODO(indexOfItem)).subscribe(state => {
    });  
  }
}
