import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpHeaders, HttpClient,  HttpRequest,  HttpHandler, HttpEvent,  HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import {AppConfig} from './utils/app-config.module';
@Injectable()
export class ApiService {
  
  hostUrl: any               = AppConfig.apiEndPoint;

	urlToFetch: any = this.hostUrl + 'fetch-to-do-list';
	urlToAdd:any    = this.hostUrl + 'add-to-do-list';
	urlToUpdate:any = this.hostUrl + 'update-to-do-list';
	urlToRemove:any = this.hostUrl + 'remove-to-do-list/';

  constructor(private _http: HttpClient) {}

  someApiCall() {
    return of([]).pipe(tap(() => console.log('log from service')));
  }

  fetchToDoList()  {
    return this._http.get(this.urlToFetch).pipe(tap((res: Response) => res));
  }

  addToDoListItem(toDoListItemContent:any)  {
      return this._http.post(this.urlToAdd, toDoListItemContent).pipe(tap((res: Response) => res));
  }

  updateToDoListItem(toDoListItemInfo:any){
    return this._http.post(this.urlToUpdate, toDoListItemInfo)
      .pipe(tap((res: Response) => res));
  }
  removeItemFromList(id:number){
    return this._http.get(this.urlToRemove+ id)
      .pipe(tap((res: Response) => res));
  }
}
