import { State, Action, StateContext } from '@ngxs/store';
import { ApiService } from './api.service';
import { tap } from 'rxjs/operators';


export class FETCHTODO {
  static readonly type = '[TodoList] FetchTodo';
  constructor() {
  }
}

export class ADDTODO {
  payload: TodoItem;
  static readonly type = '[TodoList] AddTodo';

  constructor(name: string) {
    this.payload = new TodoItem(name);
  }
}

export class UPDATETODO {
  payload: TodoItem;
  static readonly type = '[TodoList] UpdateTodo';

  constructor(newValueToSet: string, onIndex: number) {
    this.payload = new TodoItem(newValueToSet, onIndex);
  }
}

export class removeTodoItem {
  constructor(public indexOfItem: number) {}
}

export class REMOVETODO {
  payload: removeTodoItem;
  static readonly type = '[TodoList] RemoveTodo';

  constructor(onIndex: number) {
    this.payload = new removeTodoItem(onIndex);

  }
}

export class TodoItem {
  constructor(public content: string, public indexOfItem: number = 0) {}
}

export interface TodosStateModel {
  dataset: TodoItem[];
}

@State<TodosStateModel>({
  name: 'todos',
  defaults: {
    dataset: []
  }
})
export class TodosState {
  constructor(private service: ApiService) {}

  @Action(FETCHTODO)
  fetchTodo({ getState, setState }: StateContext<TodosStateModel>) {

    return this.service.fetchToDoList().pipe(
      tap((res: any) => {

        const state = getState();
        for (let todoItem of res.success) {
          state.dataset.push(new TodoItem(todoItem.todo_list_items, todoItem.id));
        }
        setState({
          ...state,
          dataset: [...state.dataset]
        });
      })
    );
  }

  @Action(ADDTODO)
  addTodo({ getState, setState }: StateContext<TodosStateModel>, { payload }: ADDTODO) {

    return this.service.addToDoListItem({"content" : payload.content}).pipe(
      tap((res: any) => {

        payload = new TodoItem(res.success.todo_list_items, res.success.id);
        const state = getState();
        setState({
          ...state,
          dataset: [...state.dataset, payload]
        });
      })
    );
  }

  @Action(UPDATETODO)
  updateTodo({ getState, setState }: StateContext<TodosStateModel>, { payload }: UPDATETODO) {

    return this.service.updateToDoListItem({"id" : payload.indexOfItem, "content" : payload.content}).pipe(
      tap((res: any) => {

        const state = getState();

        for (let todoItemIndex in state.dataset) {
          if (state.dataset[todoItemIndex].indexOfItem === res.success.id) {
            state.dataset[todoItemIndex] = new TodoItem(res.success.todo_list_items, res.success.id);
          }
        }

        setState({
          ...state,
          dataset: [...state.dataset]
        });
      })
    );
  }

  @Action(REMOVETODO)
  removeTodo({ getState, setState }: StateContext<TodosStateModel>, { payload }: REMOVETODO) {

    return this.service.removeItemFromList(payload.indexOfItem).pipe(
      tap((res: any) => {

        const state = getState();

        for (let todoItemIndex in state.dataset) {
          if (state.dataset[todoItemIndex].indexOfItem === payload.indexOfItem) {
            state.dataset.splice(+todoItemIndex, 1);
          }
        }

        setState({
          ...state,
          dataset: [...state.dataset]
        });
      })
    );
  }
}
